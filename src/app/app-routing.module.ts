import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LightModeComponent } from './components/light-mode/light-mode.component';

const routes: Routes = [
  {path: 'light-mode', component: LightModeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
