import { Injectable } from '@angular/core';
import { CARD, OVERVIEW } from '../components/mock-dashboard';
import { Card } from '../interface/card';
import { OverView } from '../interface/overView';

@Injectable({
  providedIn: 'root'
})
export class CardService {


  overview:OverView;
  card: Card;

  constructor() { }

  getCard(): Card[] {
    // if(this.card.followerToday = 144 ){
    //   this.card.state="color-red"
    // } else {
    //   this.overview.state ="color-green"
    // }
     return CARD;
  }

  getOverview(): OverView[] {
    // if(this.overview.percent < 50){
    //   this.overview.state="color-red"
    // } else{
    //   this.overview.state = "color-green"
    // }
    return OVERVIEW;
  }
}
