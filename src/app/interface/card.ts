export interface Card {
  socialid: string
  count: string
  followerToday: number
  type: string
  state?: string
}
