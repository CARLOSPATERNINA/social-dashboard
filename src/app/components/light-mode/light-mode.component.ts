import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/interface/card';
import { CardService } from 'src/app/services/card.service';
import { OverView } from '../../interface/overView';

@Component({
  selector: 'app-light-mode',
  templateUrl: './light-mode.component.html',
  styleUrls: ['./light-mode.component.sass']
})
export class LightModeComponent implements OnInit {

  cardList: Card[]
  overviewList: OverView[]
  backgroundCard: string = "light-background-card";
  cardCount: string = "card-count"
  ovCount: string = "ov-count"
  countFollowersColor: string = "light-count-fc";
  backgroundTopBody: string = "light-top-body";
  backgroundBottomBody: string = "light-bottom-body";
  marked = false;
  theCheckbox = false;
  socialBody: string = "social-body"
  borderTopCard: string = "border-top-card";
  title: string = "title"

  constructor(private cardService: CardService) { }

  ngOnInit(): void {
    this.getCard();
    this.getOverview();
  }


  //Acciones al ejecutar el modo Nocturno
  toggleVisibility(e) {
    this.marked = e.target.checked;
    if (this.marked == true) {
      this.backgroundCard = "dark-background-card";
      this.cardCount = "white-card-count"
      this.ovCount = "white-ov-count"
      this.socialBody = "dark-social-body"
      this.borderTopCard = "dark-border-top-card"
      this.title = "dark-title"
    } else {
      this.backgroundCard = "light-background-card";
      this.cardCount = "card-count"
      this.ovCount = "ov-count"
      this.socialBody = "social-body"
      this.borderTopCard = "border-top-card"
      this.title = "title"
    }
  }

  getCard() {
    this.cardList = this.cardService.getCard();
  }

  getOverview() {
    this.overviewList = this.cardService.getOverview();
  }
}
