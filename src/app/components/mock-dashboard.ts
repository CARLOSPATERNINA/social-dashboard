import { Card } from "../interface/card";
import { OverView } from "../interface/overView";

export const CARD: Card[] = [
  {
    socialid:"@nathanf",
    count:"1987",
    followerToday:12,
    type: "facebook",
    state:""
  },
  {
    socialid:"@nathanf",
    count:"1044",
    followerToday:99,
    type: "twitter",
    state:""
  },
  {
    socialid:"@realnathanf",
    count:"11k",
    followerToday:1099,
    type: "instagram",
    state:""
  },
  {
    socialid:"Nathan F.",
    count:"8239",
    followerToday:144,
    type: "youtube",
    state:""
  }
]

export const OVERVIEW: OverView[] = [
{
  title: "Page Views",
  icon: "facebook",
  percent: 3,
  count:"87",
  state:""
},
{
  title: "Likes",
  icon: "facebook",
  percent: 3,
  count:"52",
  state:""
},
{
  title: "Likes",
  icon: "instagram",
  percent: 3,
  count:"5462",
  state:""
},
{
  title: "Profile Views",
  icon: "instagram",
  percent: 3,
  count:"52k",
  state:""
},
{
  title: "Retweets",
  icon: "twitter",
  percent: 3,
  count:"117",
  state:""
},
{
  title: "Likes",
  icon: "twitter",
  percent: 3,
  count:"507",
  state:""
},
{
  title: "Likes",
  icon: "youtube",
  percent: 3,
  count:"107",
  state:""
},
{
  title: "Total Views",
  icon: "youtube",
  percent: 3,
  count:"1407",
  state:""
}

]
